## Installation

Using [Docker](https://www.docker.com/)

```bash
./docker/docker-start.sh
```
Above will configure and start all required services and initialize application.

Add following line to /etc/hosts on Linux or c:/Windows/System32/drivers/etc/hosts on Windows to make the virtual host of the application accessible from web browser
```bash
IP_OF_YOUR_DOCKER_MACHINE adzooma.docker
```

## Usage

[Adzooma](http://adzooma.docker:8000)


## Author
[Kamil Ćwikowski](mailto:kcwikowski@gmail.com)