<script>
    $("{{$source}}").autocomplete({
        source   : function (request, response) {
            $.ajax({
                url    : "{{route('company.autocomplete')}}",
                type   : "POST",
                data   : {
                    name: request.term
                },
                success: function (data) {
                    if (data.length > 0) {
                        response(data);
                    }
                }
            });
        },
        minLength: 1,
        autoFocus: true,
        select   : function (event, ui) {
            $("{{$destination}}").each(function (index, value) {
                $(value).val(ui.item.id);
            });

            return true;
        }
    });
</script>