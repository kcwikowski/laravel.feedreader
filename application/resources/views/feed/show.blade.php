@extends('layouts.master')

@section('title', 'Feed details')
@section('content')
    <div class="container-fluid pt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary card-outline">
                    <h3 class="text-center">Feed details</h3>
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="feed-img img-fluid"
                                 src="{{$feed->image ?? asset('dist/img/boxed-bg.jpg')}}"
                                 alt="Feed image">
                        </div>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Url</b> <a class="float-right">{{$feed->url}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Title</b> <a class="float-right">{{$feed->title}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Link</b> <a class="float-right">{{$feed->link}}</a>
                            </li>
                        </ul>

                        <form action="{{route('feed.destroy',$feed->id)}}" method="POST" class="text-center">
                            @csrf
                            @method('DELETE')
                            <a href="{{route('feed.edit',$feed->id)}}" class="btn btn-primary">
                                Edit
                            </a>
                            <button type="submit" class="btn btn-danger">
                                Delete
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-success card-outline">
                    <h3 class="text-center">Entries</h3>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-striped table-valign-middle">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Related link</th>
                                <th>Content</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($entries as $entry)
                                <tr>
                                    <td>
                                        {{$entry->getTitle()}}
                                    </td>
                                    <td>
                                        <a href="{{$entry->getLink()}}">
                                            {{$entry->getLink()}}
                                        </a>
                                    </td>
                                    <td>
                                        {!!$entry->getContent()!!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop