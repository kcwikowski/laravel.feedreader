@extends('layouts.master')

@section('title', 'Feeds list')
@section('content')
    @include('feed.list',['paginate'=>true])
@stop