@extends('layouts.master')

@section('title', $feed->exists ? 'Edit feed':'Create a new feed')
@section('content')
    <div class="container-fluid pt-3">
        <div class="card">
            <form method="POST"
                  action="{{ $feed->exists ? route('feed.update',$feed):route('feed.store') }}"
                  enctype="multipart/form-data">
                @if($feed->exists)
                    @method('PUT')
                @endif
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="url">URL</label>
                        <input class="form-control" id="url" placeholder="Enter feed URL" name="url"
                               value="{{old('url',$feed->url)}}">
                        @error('url')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@stop