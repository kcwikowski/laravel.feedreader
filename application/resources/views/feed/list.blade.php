<div class="container-fluid pt-3">
    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title">Feeds</h3>
            <div class="card-tools">
                <a href="{{route('feed.create')}}" class="btn btn-tool btn-sm">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-striped table-valign-middle">
                <thead>
                <tr>
                    <th>Logo</th>
                    <th>Title</th>
                    <th>Related link</th>
                    <th>Date added</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $feed)
                    <tr>
                        <td>
                            <img src="{{$feed->image ?? asset('dist/img/boxed-bg.jpg')}}"
                                 alt="Feed image"
                                 class="img-size-32 mr-2">
                        </td>
                        <td>
                            <a href="{{route('feed.show',$feed->id)}}">
                                {{$feed->title}}
                            </a>
                        </td>
                        <td>
                            <a href="{{$feed->link}}">
                                {{$feed->link}}
                            </a>
                        </td>
                        <td>
                            {{$feed->created_at}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if($paginate)
                <div class="card-header border-0">
                    <div class="card-tools">
                        {{$data->links()}}
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
