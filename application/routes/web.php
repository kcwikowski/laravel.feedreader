<?php

use Illuminate\Support\Facades\Route;

Route::get('/', "FeedController@index")
    ->name('home');
{//Auth
    Route::post('/register', "Authenticate@create");
    Route::get('/register', "Authenticate@register")
        ->name("register");
    Route::get('/login', "Authenticate@login")
        ->name("login");

    Route::post('/login', "Authenticate@authenticate");

    Route::get('/logout', "Authenticate@logout")
        ->middleware('auth')
        ->name("logout");
}
Route::resource('/feed', "FeedController");