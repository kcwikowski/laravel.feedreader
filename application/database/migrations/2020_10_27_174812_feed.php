<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Feed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed', function (Blueprint $table) {
            $table
                ->string('id', 36)
                ->unique()
                ->primary();
            $table->string('user_id', 36);
            $table->string('url', 512);
            $table->string('title', 255)->nullable();
            $table->string('link', 1024)->nullable();
            $table->string('image', 1024)->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
            $table->unique(
                [
                    'user_id',
                    'url',
                ]
            );
            $table
                ->foreign('user_id')
                ->references('id')->on('user')
                ->onUpdate('restrict')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed');
    }
}
