<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    const TEST_USER1 = 'admin@admin.com';
    const TEST_USER2 = 'admin2@admin.com';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new \App\User())
            ->setAttribute('email', self::TEST_USER1)
            ->setAttribute('password', 'password')
            ->save();
        (new \App\User())
            ->setAttribute('email', self::TEST_USER2)
            ->setAttribute('password', 'password')
            ->save();
    }
}
