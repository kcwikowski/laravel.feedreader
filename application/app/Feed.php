<?php

namespace App;

use App\Services\Feed\TypeInterface;
use App\Services\Feed\Type\Rss;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class Feed extends Model
{
    public    $incrementing = false;
    protected $table        = 'feed';
    protected $fillable     = [
        'url',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function (self $feed) {
            if (!$feed->id) {
                $feed->id = Uuid::uuid4()->toString();
            }
        });
        static::saving(function (self $feed) {
            if (!$feed->user_id) {
                $feed->user_id = Auth::id();
            }
        });
    }

    /**
     * @param TypeInterface $feed
     * @return bool
     */
    public function hydrateFromReader(TypeInterface $feed): bool
    {
        $this->title = $feed->getTitle();
        $this->link  = $feed->getLink();
        if ($feed instanceof Rss) {
            $this->description = $feed->getDescription();
            $this->image       = $feed->getImage();
        }

        return true;
    }
}
