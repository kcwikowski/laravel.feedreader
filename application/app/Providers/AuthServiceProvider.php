<?php

namespace App\Providers;

use App\Feed;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define(
            Feed::class,
            function ($user, $feed) {
                return
                    is_null($feed->user_id)
                    || $user->id == $feed->user_id;
            }
        );
    }
}
