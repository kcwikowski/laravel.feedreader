<?php

namespace App\Http\Controllers;

use App\Feed;
use App\Services\Feed\Reader;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class FeedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data =
            Feed::query()
                ->where('user_id', Auth::id())
                ->paginate(10);

        return
            view('feed.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return
            $this->edit(new Feed());
    }

    /**
     * @param \App\Http\Requests\Feed $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\Feed $request)
    {
        $message = 'Error occurred when adding a new feed';
        $feed    = new Feed($request->validated());

        if ($reader = Reader\Wrapper\Xml\Url::fromUrl($feed->url)) {
            if (
                $feed->hydrateFromReader($reader)
                && $feed->save()
            ) {
                return
                    redirect()
                        ->route(
                            'feed.show',
                            $feed,
                            302,
                            [
                                'X-Created' => 1,
                            ]
                        )
                        ->with('success', 'Feed added.');
            }
        } else {
            $message = 'Provided URL failed to produce a valid feed';
        }

        $request->flash();

        return
            redirect()
                ->route('feed.create')
                ->with('error', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feed $feed
     * @return \Illuminate\Http\Response
     */
    public function show(Feed $feed)
    {
        $this->authorizeFeed($feed);
        $entries =
            Reader\Wrapper\Xml\Url::fromUrl($feed->url)->getEntries();

        return
            view(
                'feed.show',
                compact('feed', 'entries')
            );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feed $feed
     * @return \Illuminate\Http\Response
     */
    public function edit(Feed $feed)
    {
        $this->authorizeFeed($feed);

        return
            view(
                'feed.edit',
                compact('feed')
            );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Feed $request
     * @param  \App\Feed $feed
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\Feed $request, Feed $feed)
    {
        $this->authorizeFeed($feed);
        $message = 'Feed not updated';
        $feed->fill($request->validated());

        if ($reader = Reader\Wrapper\Xml\Url::fromUrl($feed->url)) {
            if (
                $feed->hydrateFromReader($reader)
                && $feed->save()
            ) {

                return
                    redirect()
                        ->route(
                            'feed.show',
                            $feed,
                            302,
                            [
                                'X-Updated' => 1,
                            ]
                        )
                        ->with('success', 'Feed updated');
            }
        } else {
            $message = 'Provided URL failed to produce a valid feed';
        }

        return
            redirect()
                ->route('feed.edit', $feed)
                ->with('error', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feed $feed
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feed $feed)
    {
        $this->authorizeFeed($feed);

        if ($feed->delete()) {
            return
                redirect()
                    ->route(
                        'feed.index',
                        [],
                        302,
                        [
                            'X-Deleted' => 1,
                        ]
                    )
                    ->with('success', 'Feed deleted');
        }

        return
            redirect()
                ->route('feed.show', $feed)
                ->with('error', 'Feed not deleted');
    }

    /**
     * @param Feed $feed
     */
    protected function authorizeFeed(Feed $feed)
    {
        if (Gate::denies(Feed::class, $feed)) {
            abort(404);
        }
    }
}
