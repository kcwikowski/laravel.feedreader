<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Register extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
            [
                'email'           => 'required|email|unique:user',
                'password'        => 'required|string|min:3|max:255',
                'password_repeat' => 'required|same:password',
            ];
    }
}
