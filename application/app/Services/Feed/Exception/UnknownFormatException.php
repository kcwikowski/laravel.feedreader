<?php

namespace App\Services\Feed\Exception;

use App\Services\Feed\Exception;

class UnknownFormatException extends Exception
{
}
