<?php

namespace App\Services\Feed;

abstract class AbstractType extends AbstractFeedData implements TypeInterface
{
    /**
     * @var AbstractEntry[]
     */
    protected $entries = [];

    /**
     * @return AbstractEntry[]
     */
    public function &getEntries(): array
    {
        return $this->entries;
    }

    /**
     * @param AbstractEntry[] $entries
     * @return static
     */
    public function setEntries(array $entries)
    {
        $this->entries = $entries;

        return $this;
    }
}
