<?php

namespace App\Services\Feed\Entry;

use App\Services\Feed\AbstractEntry;

class Atom extends AbstractEntry
{
    /**
     * @var string
     */
    protected $summary;

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->getSummary();
    }

    /**
     * @return string
     */
    public function getSummary(): string
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     * @return static
     */
    public function setSummary(string $summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * @return bool
     */
    protected function parse(): bool
    {
        if (!(
            $this->getData()->title
            && $this->getData()->link
            && isset($this->getData()->link['href'])
        )) {

            return false;
        }

        $this->setTitle(
            (string)$this->getData()->title
        );
        $this->setLink(
            (string)$this->getData()->link['href']
        );

        if ($this->getData()->summary) {

            $this->setSummary(
                (string)$this->getData()->summary->children()
            );
        }

        return true;
    }
}
