<?php

namespace App\Services\Feed\Entry;

use App\Services\Feed\AbstractEntry;

class Rss extends AbstractEntry
{
    /**
     * @var string
     */
    protected $description;

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return static
     */
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return
            $this->getDescription();
    }

    /**
     * @return bool
     */
    protected function parse(): bool
    {
        if (!$this->getData()->xpath('title')) {
            return false;
        }

        $this->setTitle(
            (string)$this->getData()->xpath('title')[0]
        );

        if ($this->getData()->xpath('link')) {
            $this->setLink(
                (string)$this->getData()->xpath('link')[0]
            );
        }

        if ($this->getData()->xpath('description')) {
            $this->setDescription(
                (string)$this->getData()->xpath('description')[0]
            );
        }

        return true;
    }
}
