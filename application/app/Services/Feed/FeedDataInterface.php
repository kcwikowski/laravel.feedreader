<?php

namespace App\Services\Feed;

interface FeedDataInterface
{

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return string
     */
    public function getLink(): string;
}
