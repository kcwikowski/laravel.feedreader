<?php

namespace App\Services\Feed;

interface XmlDataInterface
{
    /**
     * @return \SimpleXMLElement
     */
    public function getData(): ?\SimpleXMLElement;

    /**
     * @param \SimpleXMLElement $data
     * @return static
     */
    public function setData(\SimpleXMLElement $data);
}
