<?php namespace App\Services\Feed;

use App\Services\Feed\Exception\UnknownFormatException;
use App\Services\Feed\Exception\UnknownTypeException;

use App\Services\Feed\Type\Atom;
use App\Services\Feed\Type\Rss;

interface ReaderInterface
{
    const TYPE_RSS  = 'rss';
    const TYPE_ATOM = 'atom';
    const TYPES     = [
        self::TYPE_RSS  => Rss::class,
        self::TYPE_ATOM => Atom::class,
    ];

    /**
     * @param mixed $source
     * @return TypeInterface|null
     * @throws UnknownTypeException
     * @throws UnknownFormatException
     */
    public static function create($source): ?TypeInterface;
}