<?php

namespace App\Services\Feed;

use App\Services\Feed\Exception\InvalidXmlDataException;

abstract class AbstractXmlData implements XmlDataInterface
{
    /**
     * @var \SimpleXMLElement
     */
    protected $data;

    /**
     * AbstractXmlData constructor.
     * @param \SimpleXMLElement $data
     * @throws InvalidXmlDataException
     */
    public function __construct(\SimpleXMLElement $data)
    {
        $this->setData($data);
        if (!$this->parse()) {
            throw new InvalidXmlDataException();
        }
    }

    /**
     * @return \SimpleXMLElement
     */
    public function getData(): ?\SimpleXMLElement
    {
        return $this->data;
    }

    /**
     * @param \SimpleXMLElement $data
     * @return static
     */
    public function setData(\SimpleXMLElement $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return bool
     */
    abstract protected function parse(): bool;
}
