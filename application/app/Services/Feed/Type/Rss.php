<?php

namespace App\Services\Feed\Type;

use App\Services\Feed\AbstractType;

class Rss extends AbstractType
{
    /**
     * @var string
     */
    protected $image;
    /**
     * @var string
     */
    protected $description;

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return static
     */
    public function setImage(string $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return static
     */
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool
     */
    protected function parse(): bool
    {
        if (!$this->getData()->xpath('/rss/channel/title')) {
            return false;
        }
        $this->setTitle(
            (string)$this->getData()->xpath('/rss/channel/title')[0]
        );
        if ($this->getData()->xpath('/rss/channel/link')) {
            $this->setLink(
                (string)$this->getData()->xpath('/rss/channel/link')[0]
            );
        }
        if ($this->getData()->xpath('/rss/channel/image/url')) {
            $this->setImage(
                (string)$this->getData()->xpath('/rss/channel/image/url')[0]
            );
        }
        if ($this->getData()->xpath('/rss/channel/description')) {
            $this->setDescription(
                (string)$this->getData()->xpath('/rss/channel/description')[0]
            );
        }

        $this->setEntries([]);

        foreach ($this->getData()->xpath('/rss/channel/item') as $entry) {
            if ($entryInstance = new \App\Services\Feed\Entry\Rss($entry)) {
                $this->getEntries()[] = $entryInstance;
            }
        }

        return true;
    }
}
