<?php

namespace App\Services\Feed\Type;

use App\Services\Feed\AbstractType;
use App\Services\Feed\Exception\InvalidXmlDataException;

class Atom extends AbstractType
{
    protected function parse(): bool
    {
        if (!(
            $this->getData()->title
            && $this->getData()->link
            && isset($this->getData()->link['href'])
        )) {
            return false;
        }

        $this->setTitle(
            (string)$this->getData()->title
        );
        $this->setLink(
            (string)$this->getData()->link['href']
        );

        $this->setEntries([]);

        if ($this->getData()->entry) {
            foreach ($this->getData()->entry as $entry) {
                try {
                    if ($entryInstance = new \App\Services\Feed\Entry\Atom($entry)) {
                        $this->getEntries()[] = $entryInstance;
                    }
                } catch (InvalidXmlDataException $e) {
                    continue;
                }
            }
        }

        return true;
    }
}
