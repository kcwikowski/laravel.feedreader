<?php

namespace App\Services\Feed;

interface EntryInterface extends FeedDataInterface
{
    /**
     * @return string
     */
    public function getContent(): string;
}
