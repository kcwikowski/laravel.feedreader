<?php

namespace App\Services\Feed;

abstract class AbstractFeedData extends AbstractXmlData
{
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string
     */
    protected $link;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return static
     */
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return static
     */
    public function setLink(string $link)
    {
        $this->link = $link;

        return $this;
    }
}
