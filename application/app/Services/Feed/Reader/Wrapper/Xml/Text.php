<?php

namespace App\Services\Feed\Reader\Wrapper\Xml;

use App\Services\Feed\Reader\Xml;
use App\Services\Feed\TypeInterface;

class Text extends Xml
{
    /**
     * @param string $string
     * @return TypeInterface|null
     */
    public static function fromString(string $string): ?TypeInterface
    {
        if (!$data = @simplexml_load_string($string)) {
            return null;
        }

        return
            self::create($data);
    }
}
