<?php

namespace App\Services\Feed\Reader\Wrapper\Xml;

use App\Services\Feed\Reader\Xml;
use App\Services\Feed\TypeInterface;

class Url extends Xml
{
    /**
     * @param string $url
     * @return TypeInterface|null
     */
    public static function fromUrl(string $url): ?TypeInterface
    {
        if (!$data = @simplexml_load_file($url)) {
            return null;
        }

        return
            self::create($data);
    }
}
