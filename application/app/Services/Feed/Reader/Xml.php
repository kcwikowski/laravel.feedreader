<?php

namespace App\Services\Feed\Reader;

use App\Services\Feed\Exception\InvalidXmlDataException;
use App\Services\Feed\Exception\UnknownTypeException;
use App\Services\Feed\ReaderInterface;
use App\Services\Feed\TypeInterface;
use Carbon\Exceptions\InvalidFormatException;

class Xml implements ReaderInterface
{
    /**
     * @param \SimpleXMLElement $feed
     * @return TypeInterface|null
     * @throws UnknownTypeException
     */
    public static function fromXml(\SimpleXMLElement $feed): ?TypeInterface
    {

        if (!array_key_exists(
            $type = self::getFeedType($feed),
            self::TYPES
        )) {
            throw new UnknownTypeException();
        }

        try {
            $typeClass    = self::TYPES[$type];
            $feedInstance = new $typeClass($feed);
        } catch (InvalidXmlDataException $e) {
            return null;
        }

        return $feedInstance;
    }

    /**
     * @param mixed $source
     * @return TypeInterface|null
     */
    public static function create($source): ?TypeInterface
    {
        if (!$source instanceof \SimpleXMLElement) {
            throw new InvalidFormatException('Expected SimpleXmlElement object');
        }

        return
            self::fromXml($source);
    }

    /**
     * @param \SimpleXMLElement $feed
     * @return null|string
     */
    protected static function getFeedType(\SimpleXMLElement $feed): ?string
    {
        switch (true) {
            case $feed->xpath('/rss'):
                //TODO:Add version detection
                return self::TYPE_RSS;
                break;
            case (
                'feed' === $feed->getName()
                && 'http://www.w3.org/2005/Atom' === $feed->getNamespaces()['']
            ):
                return self::TYPE_ATOM;
                break;
            default:
                return null;
        }
    }
}
