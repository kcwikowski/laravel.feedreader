<?php

namespace App\Services\Feed;

interface TypeInterface extends FeedDataInterface
{
    /**
     * @return AbstractEntry[]
     */
    public function &getEntries(): array;
}
