<?php

namespace Tests;

use App\User;

abstract class AbstractAuthTestCase extends TestCase
{
    use CreatesApplication;

    /**
     * @param string|null $email
     * @return $this
     */
    protected function initActor(string $email = null)
    {
        if (is_null($email)) {
            $email = \UserSeeder::TEST_USER1;
        }
        $user =
            User::query()
                ->where('email', '=', $email)
                ->get()
                ->first();

        return
            $this->actingAs($user);
    }
}
