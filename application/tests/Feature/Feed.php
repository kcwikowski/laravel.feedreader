<?php

namespace Tests\Feature;

use App\Services\Feed\Reader\Wrapper\Xml\Text;
use App\Services\Feed\Type\Atom;
use App\Services\Feed\Type\Rss;
use Ramsey\Uuid\Uuid;
use Tests\AbstractAuthTestCase;

class Feed extends AbstractAuthTestCase
{
    public function testAuth()
    {
        $this
            ->get('/feed')
            ->assertStatus(302);
    }

    public function testIndex()
    {
        $this
            ->initActor()
            ->get('/feed')
            ->assertStatus(200);
    }

    public function testCreate()
    {
        $this
            ->initActor()
            ->get('/feed/create')
            ->assertStatus(200)
            ->assertViewIs('feed.edit');
    }

    public function testStore()
    {
        $this
            ->initActor()
            ->post(
                '/feed',
                [
                    'url' => 'https://www.feedforall.com/sample-feed.xml?r=' . Uuid::uuid4()->toString(),
                ])
            ->assertStatus(302)
            ->assertHeader('X-Created', 1);
    }

    public function testStoreInvalid()
    {
        $this
            ->initActor()
            ->post(
                '/feed',
                [
                    'url' => 'https://no.feeds.here',
                ])
            ->assertStatus(302)
            ->assertHeaderMissing('X-Created');
    }

    public function testShow()
    {
        $this->initActor();

        $this
            ->get('/feed/' . $this->getFeed()->id)
            ->assertStatus(200);
    }

    public function testHorizontalAuthorization()
    {

        /**
         * @var \App\Feed $feed
         */
        $feed =
            \App\Feed::query()
                ->first()
                ->get()
                ->first();

        $this
            ->initActor(\UserSeeder::TEST_USER2)
            ->get('/feed/' . $feed->id)
            ->assertStatus(404);
    }

    public function testShowInvalid()
    {
        $this->initActor();

        $this
            ->get('/feed/' . Uuid::uuid4()->toString())
            ->assertStatus(404);
    }

    public function testEdit()
    {
        $this->initActor();

        $this
            ->get('/feed/' . $this->getFeed()->id . '/edit')
            ->assertStatus(200);
    }

    public function testEditInvalid()
    {
        $this->initActor();

        $this
            ->get('/feed/' . Uuid::uuid4()->toString() . '/edit')
            ->assertStatus(404);
    }

    public function testUpdate()
    {
        $this->initActor();

        $this
            ->put(
                '/feed/' . $this->getFeed()->id,
                [
                    'url' => 'https://www.feedforall.com/sample.xml?r=' . Uuid::uuid4()->toString(),
                ])
            ->assertStatus(302)
            ->assertHeader('X-Updated', 1);
    }

    public function testUpdateInvalid()
    {
        $this->initActor();

        $this
            ->put(
                '/feed/' . $this->getFeed()->id,
                [
                    'url' => 'https://no.feeds.here',
                ])
            ->assertStatus(302)
            ->assertHeaderMissing('X-Updated');
    }

    public function testDestroy()
    {
        $this->initActor();

        $this
            ->delete('/feed/' . $this->getFeed()->id)
            ->assertStatus(302)
            ->assertHeader('X-Deleted', 1);
    }

    public function testDestroyInvalid()
    {
        $this->initActor();

        $this
            ->delete('/feed/' . Uuid::uuid4()->toString())
            ->assertStatus(404)
            ->assertHeaderMissing('X-Deleted');
    }

    public function testRssFeedFromString()
    {
        $feed   =
            file_get_contents(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        __DIR__,
                        'Feed',
                        'Rss.xml',
                    ]
                )
            );
        $reader = Text::fromString($feed);

        $this->assertNotNull($reader);
        $this->assertInstanceOf(Rss::class, $reader);
        $this->assertCount(9, $reader->getEntries());
        $this->assertNotEmpty($reader->getTitle());
        $this->assertNotEmpty($reader->getLink());
        $this->assertNotEmpty($reader->getEntries()[0]->getTitle());
        $this->assertNotEmpty($reader->getEntries()[0]->getLink());
        $this->assertNotEmpty($reader->getEntries()[0]->getContent());
    }

    public function testAtomFeedFromString()
    {
        $feed   =
            file_get_contents(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        __DIR__,
                        'Feed',
                        'Atom.xml',
                    ]
                )
            );
        $reader = Text::fromString($feed);

        $this->assertNotNull($reader);
        $this->assertInstanceOf(Atom::class, $reader);
        $this->assertCount(3, $reader->getEntries());
        $this->assertNotEmpty($reader->getTitle());
        $this->assertNotEmpty($reader->getLink());
        $this->assertNotEmpty($reader->getEntries()[0]->getTitle());
        $this->assertNotEmpty($reader->getEntries()[0]->getLink());
        $this->assertNotEmpty($reader->getEntries()[0]->getContent());
    }

    public function testFeedFromStringInvalid()
    {
        $reader = Text::fromString(Uuid::uuid4()->toString());
        $this
            ->assertNull($reader);
    }

    /**
     * @return \App\Feed
     */
    protected function getFeed()
    {
        return
            \App\Feed::query()
                ->first()
                ->get()
                ->first();
    }
}
