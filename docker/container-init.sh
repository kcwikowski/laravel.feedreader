#!/bin/bash

# ------------------------------------------------
# Variables
# ------------------------------------------------
APP_NAME="adzooma"
LOCK_FILE="container.lock"
DATE_TIMEZONE=Europe/London
if [ -d ${LOCK_FILE} ]; then
    echo "Other container-init process is active"
    exit 1
fi
date > ${LOCK_FILE}
# ------------------------------------------------
# Managing needed modules
# ------------------------------------------------

# ------------------------------------------------
# Prepare php.ini
# ------------------------------------------------
sed -i 's|^;date.timezone =|date.timezone = '${DATE_TIMEZONE}'|' /etc/php/7.4/apache2/php.ini
sed -i 's|^;date.timezone =|date.timezone = '${DATE_TIMEZONE}'|' /etc/php/7.4/cli/php.ini

# ------------------------------------------------
# Start MySQL
# ------------------------------------------------
service mysql restart

# ------------------------------------------------
# Create users & grant permissions
# ------------------------------------------------
mysql -e "DROP DATABASE IF EXISTS ${APP_NAME}"
mysql -e "DROP DATABASE IF EXISTS ${APP_NAME}_test"
mysql -e "DROP USER IF EXISTS ${APP_NAME}"
mysql -e "FLUSH PRIVILEGES;"
mysqladmin flush-privileges
mysql -e "CREATE DATABASE ${APP_NAME} CHARACTER SET utf8 COLLATE utf8_bin"
mysql -e "CREATE DATABASE ${APP_NAME}_test CHARACTER SET utf8 COLLATE utf8_bin"
mysql -e "CREATE USER ${APP_NAME}@'%' IDENTIFIED BY PASSWORD '*67329805F6F9E49A853250B4B2D9E815B8DE7F74';" #Giq5rE56N7kikal6piDe6iY423c5tO
mysql -e "GRANT ALL ON ${APP_NAME}.* TO ${APP_NAME}@'%';"
mysql -e "GRANT ALL ON ${APP_NAME}_test.* TO ${APP_NAME}@'%';"
mysql -e "FLUSH PRIVILEGES;"

# ------------------------------------------------
# Prepare project
# ------------------------------------------------
composer install --no-interaction
# ------------------------------------------------
# Restart Apache service
# ------------------------------------------------
a2ensite ${APP_NAME}
service apache2 reload

# ------------------------------------------------
echo Initialization finished
rm -rf ${LOCK_FILE}